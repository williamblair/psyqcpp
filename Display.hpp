/*
 * Display.hpp
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>

#ifndef DISPLAY_H_INCLUDED
#define DISPLAY_H_INCLUDED

class Display {
public:
	Display();
	~Display();
	
	void init(int w, int h);
	
	void set_buffers();
	
	void update();
	
private:
	DRAWENV draw[2];
	DISPENV disp[2];
	
	int width, height;
	int frame_counter;
	int cur_buffer;
};

#endif // DISPLAY_H_INCLUDED

 