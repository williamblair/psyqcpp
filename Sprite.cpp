/*
 * Sprite.cpp
 */

#include "Sprite.hpp"

Sprite::Sprite() {
	width = 0; height = 0;
	clip_x = 0; clip_y = 0;
	
	tex_id = 0;
	clut_id = 0;
	
	anim_speed = 20;
	anim_count = 0;

    clut_x = 0; clut_y = 0;
}

// this should only happen once since the members are static
// vram_texture_x might need to variably change based on 
// if the screen is 
int Sprite::vram_texture_x = 640; 
int Sprite::vram_texture_y = 0;

int Sprite::vram_clut_x = 0;
int Sprite::vram_clut_y = 500;


// optional clut ID for reusing a clut if desired
void Sprite::load(u_long *texture, u_long *clut, int tex_w, int tex_h, 
                  u_short existing_clut_id = (u_short)(-1),
				  int bit_depth = -1) {

	SetPolyFT4(&poly);
	SetShadeTex(&poly, 1); // shadtex disable
	
	tex_width = tex_w; tex_height = tex_h;
	
	pos.vx = 0; pos.vy = 0;
	
	// figure out where to upload the texture
	if (tex_w + vram_texture_x > 1024) {
		vram_texture_x = 320-tex_w;
		
		// TODO - this should actually be += (tallest texture uploaded)
		vram_texture_y += tex_h;
	}
	
	printf("Vram texture x, y: %d,%d\n", vram_texture_x, vram_texture_y);
    printf("Bit depth: %d\n", bit_depth);

    if (bit_depth == -1) bit_depth = 1;
	
	// load the texture data into vram
	tex_id = LoadTPage(
		texture, 
		bit_depth,                          // Bit Depth - 0: 4bit, 1: 8bit, 2: 16bit
		0,                                  // semi transparency rate
		vram_texture_x,vram_texture_y,      // x,y within framebuffer
		tex_w, tex_h
	);
	
	// update the x pos in framebuffer
	vram_texture_x += tex_w;
	
	// https://stackoverflow.com/questions/2403631/how-do-i-find-the-next-multiple-of-10-of-any-integer#2403643
	// round up to nearest texture page (64px width)
	if (vram_texture_x % 64) {
		printf("vram texture x before: %d\n", vram_texture_x);
		vram_texture_x += (64 - vram_texture_x % 64);
		printf("vram_texture_x after: %d\n", vram_texture_x);
	}
	
	// load the clut (if any) into vram
	// TODO - automate the x/y position upload
	if (clut) {
		clut_id = LoadClut(clut, vram_clut_x, vram_clut_y);

        // keep a copy of the clut coordinates for this sprite
        clut_x = vram_clut_x; clut_y = vram_clut_y;

		// TODO - do some bounds checking for this
		vram_clut_y++;

	} else if ((short)existing_clut_id != -1) {
		printf("Using existing CLUT!\n");
		clut_id = existing_clut_id;
	}
	
	// set the texture ids in the polygon
	poly.tpage = tex_id;
	if (clut_id != 0 && (short)clut_id != -1) poly.clut = clut_id;
	
	// set the texture point
	setUVWH(&poly, 0,0, tex_width, tex_height);
	
	// set the location of the sprite
	setXYWH(&poly, 0,0, tex_width, tex_height);
}

void Sprite::animate(void) {
	
	anim_count++;
	
	// switch to the next clip if necessary
	if (anim_count >= anim_speed) {
		anim_count = 0;
		
		clip_x += width;
		if (clip_x >= tex_width) {
			clip_x = 0; clip_y += height;
			
			if (clip_y >= tex_height) {
				clip_y = 0;
			}
		}
		
		setUVWH(&poly, clip_x, clip_y, width, height);
	}
}

void Sprite::draw(void) {
	DrawPrim(&poly);
}

void Sprite::setX(int x) {
	pos.vx = x;
	setXYWH(&poly, pos.vx, pos.vy, width, height);
	setUVWH(&poly, clip_x, clip_y, width, height);
}

void Sprite::setY(int y) {
	pos.vy = y;
	setXYWH(&poly, pos.vx, pos.vy, width, height);
	setUVWH(&poly, clip_x, clip_y, width, height);
}

// set the ACTUAL width and height of the sprite, not its
// texture size
void Sprite::setW(int w) {
	width = w;
	setXYWH(&poly, pos.vx, pos.vy, width, height);
	setUVWH(&poly, clip_x, clip_y, width, height);
}
void Sprite::setH(int h) {
	height = h;
	setXYWH(&poly, pos.vx, pos.vy, width, height);
	setUVWH(&poly, clip_x, clip_y, width, height);
}

// set the clip location within the texture
void Sprite::setU(int u) {
	clip_x = u;
	setUVWH(&poly, clip_x, clip_y, width, height);
}
void Sprite::setV(int v) {
	clip_y = v;
	setUVWH(&poly, clip_x, clip_y, width, height);
}

u_short Sprite::get_clut_id(void)    { return clut_id; }
u_short Sprite::get_texture_id(void) { return tex_id; }

void Sprite::load_clut(u_long *clut)
{
    clut_id = LoadClut(clut, clut_x, clut_y);
}

