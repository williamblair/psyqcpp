/*
 * Display.cpp
 */

#include "Display.hpp"

Display::Display() {
	width = 0; height = 0;
	frame_counter = 0;
	cur_buffer = 0;
}
Display::~Display() {
	
}

void Display::init(int w, int h) {
	
	// reset the controller
	PadInit(0);
	
	// reset graphics subsystem
	ResetGraph(0);
	
	// Load the bios font
	FntLoad(960,256);
	SetDumpFnt(FntOpen(16,16,256,64,0,512));
	
	// set locations of draw/display envs for double buffer
	SetDefDrawEnv(&draw[0], 0, 0, w, h);
	SetDefDispEnv(&disp[0], 0, h, w, h);
	SetDefDrawEnv(&draw[1], 0, h, w, h);
	SetDefDispEnv(&disp[1], 0, 0, w, h);
	
	// auto clear bg to bg color
	draw[0].isbg = draw[1].isbg = 1;
	setRGB0(&draw[0], 0,100,255);
	setRGB0(&draw[1], 0,100,255);
	
	// display enable
	SetDispMask(1);
	
	width = w; height = h;
}

void Display::set_buffers() {
	// Set where to draw and display
	PutDrawEnv(&draw[cur_buffer]);
	PutDispEnv(&disp[cur_buffer]);
	
	// switch buffer indicator
	cur_buffer = !cur_buffer;
}

void Display::update() {
	FntFlush(-1);
	VSync(0);
}