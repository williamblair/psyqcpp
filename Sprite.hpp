/*
 * Sprite.hpp
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>

#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

class Sprite {
public:

	Sprite();

    // optional clut ID for reusing a clut if desired
	void load(u_long *texture, u_long *clut, int tex_w, int tex_h, 
			 u_short existing_clut_id = (u_short)(-1),
			 int bit_depth = -1);
	
	void draw(void);
	
	void setX(int x);
	void setY(int y);
	
	// set the ACTUAL width and height of the sprite, not its
	// texture size
	void setW(int w);
	void setH(int h);
	
	// set the clip location within the texture
	void setU(int u);
	void setV(int v);
	
	// increment the animator frame count and move to the 
	// next sprite clip if necessary
	void animate(void);

    u_short get_clut_id(void);
    u_short get_texture_id(void);

    // upload a different clut (assumes load() has already been called with clut)
    void load_clut(u_long *clut);

private:
	POLY_FT4 poly; // texture mapped polygon
	SVECTOR  pos;  // position/clip within texture
	
	// keep track of the x and y coordinates used by EACH
	// sprite instance, so we can figure out where to upload
	// textures in VRAM without overlapping each other
	static int vram_texture_x;
	static int vram_texture_y;

    // keep track of the x and y coordinates of cluts used
    // by EACH sprite instance, so we can figure out where
    // to upload cluts without overlapping each other
    static int vram_clut_x;
    static int vram_clut_y;
	
	u_short tex_id;  // texture id
	u_short clut_id; // clut id
	
	// size of the texture used by the sprite
	int tex_width, tex_height;
	
	// size of the sprite to be drawn
	int width, height;
	int clip_x,clip_y;  // offset/clip within texture

    // copy of where the clut was loaded in vram
    int clut_x, clut_y;
	
	// how many frames until the next sprite clip is used
	// and how many frames its been since the last clip changed
	int anim_speed;
	int anim_count;
};

#endif // SPRITE_H_INCLUDED
